name := """kafkabox"""

version := "1.0"

scalaVersion := "2.11.7"

// Change this to another test framework if you prefer
libraryDependencies ++= List(
  "org.apache.kafka" % "kafka-clients" % "0.10.0.0"
    exclude("org.slf4j", "slf4j-log4j12")
    exclude("javax.jms", "jms")
    exclude("com.sun.jdmk", "jmxtools")
    exclude("com.sun.jmx", "jmxri"),

  "org.javaswift" % "joss" % "0.9.7",
  "org.springframework.boot" % "spring-boot-starter" % "1.3.3.RELEASE",
  "javax.inject" % "javax.inject" % "1",
  "com.github.fommil" %% "spray-json-shapeless" % "1.2.0"
)

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"

