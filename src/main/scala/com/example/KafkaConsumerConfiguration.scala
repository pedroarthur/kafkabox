package com.example

import java.io.File
import java.nio.file.{Files, Path, Paths}
import java.util
import java.util.Properties
import java.util.concurrent.{Executors, TimeUnit}
import javax.annotation.PostConstruct
import javax.inject.Inject

import com.example.StorageOperations.{CREATE, DELETE}
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.StringDeserializer
import org.javaswift.joss.model.StoredObject
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.stereotype.Component

import collection.JavaConversions._
import scala.util.{Failure, Success, Try}
import Function.tupled
import scala.concurrent.ExecutionContext

/**
  * TODO: a wonderful description about this nice piece of work!
  */
@Configuration class KafkaConsumerConfiguration {
  @Value("${kafka.bootstrap.servers:localhost:9092}") val bootstrapServers = ""
  @Value("${kafka.consumer.group.id:file}") val groupID = "file"

  @Bean def consumer() = new KafkaConsumer[String, String](new Properties() {
    put("bootstrap.servers", bootstrapServers)
    put("group.id", groupID)
    put("enable.auto.commit", "true")
    put("auto.commit.interval.ms", "1000")
    put("session.timeout.ms", "30000")
    put("key.deserializer", classOf[StringDeserializer].getName)
    put("value.deserializer", classOf[StringDeserializer].getName)
  })

  @Bean def groupId = ConsumerParameters(groupID)
}

case class ConsumerParameters(groupId: String)

@Component class KafkaSubscription @Inject() (
  consumer: KafkaConsumer[String, String], consumerLoop: ConsumerLoop,
  topicConfiguration: KafkaTopicConfiguration) {

  val executor = Executors.newScheduledThreadPool(2)

  @PostConstruct def subscribe() = {
    consumer.subscribe(new util.LinkedList[String]() { add(topicConfiguration.topic) })
    executor.scheduleAtFixedRate(consumerLoop, 0, 100, TimeUnit.MILLISECONDS)
  }
}

@Component class ConsumerLoop @Inject() (
  parameters: ConsumerParameters, consumer: KafkaConsumer[String, String],
  storageSynchronizer: StorageSynchronizer) extends Runnable {

  val logger = LoggerFactory.getLogger(classOf[ConsumerLoop])

  import spray.json._
  import StorageOperationProtocol._

  override def run(): Unit =
    Try {
      consumer.poll(100) map {
        r => (r.key(), r.value().parseJson.convertTo[StorageOperation])
      } filter tupled {
        (_, operation) => operation.groupId != parameters.groupId
      } foreach tupled {
        (key, value) => logger.info(s"received $key: $value")
          storageSynchronizer.synchronize(key, value)
      }
    } match {
      case Failure(f) => logger.error("while fetching", f)
      case Success(_) =>
    }
}

trait StorageSynchronizer {
  def synchronize(objectName: String, operation: StorageOperation)
}

trait SynchronizationHandler {
  def handle(storedObject: StoredObject, file: File): Unit
}

@Component class CreateHandler @Inject()(baseDirectory: Path) extends SynchronizationHandler {
  val logger = LoggerFactory.getLogger(classOf[CreateHandler])

  def handle(createdObject: StoredObject, file: File): Unit = {
    if (!createdObject.exists()) {
      throw new IllegalStateException(s"object ${createdObject.getPath} does not exist")
    }

    logger.info(s"file ${createdObject.getName} will be stored at ${file.getAbsolutePath}")

    if (file.exists()) {
      throw new IllegalStateException(s"file ${file.getAbsolutePath} already exist! Skipping download")
    }

    createdObject.downloadObject(file)
  }
}

@Component class DeleteHandler @Inject()(baseDirectory: Path) extends SynchronizationHandler {
  val logger = LoggerFactory.getLogger(classOf[CreateHandler])

  def handle(deleted: StoredObject, file: File): Unit = {
    if (deleted.exists()) {
      throw new IllegalStateException(s"object ${deleted.getPath} still in the server")
    }

    if (file.delete()) {
      logger.info(s"file ${file.getAbsolutePath} was deleted")
    } else {
      logger.warn(s"file ${file.getAbsoluteFile} was already deleted")
    }
  }
}

@Component class DelegatingStorageSynchronizer @Inject()(ignoreCache: IgnoreCache, basePath: Path,
  objectStore: ObjectStore, createHandler: CreateHandler, deleteHandler: DeleteHandler)
  extends StorageSynchronizer {

  implicit val ec: ExecutionContext =
    ExecutionContext.fromExecutor(Executors.newFixedThreadPool(2))

  val logger = LoggerFactory.getLogger(classOf[DelegatingStorageSynchronizer])

  override def synchronize(objectName: String, operation: StorageOperation): Unit = {
    logger.info(s"Handling $objectName with $operation")

    val target = basePath.resolve(objectName).toAbsolutePath
    val file = new File(target.toString)

    ignoreCache.ignoreOnce(target.toString)

    objectStore.get(objectName) andThen {
      case Success(storedObject) => handlerFor(operation).handle(storedObject, file)
      case Failure(f) => logger.error(s"while handling $objectName", f)
    }
  }

  def handlerFor(operation: StorageOperation) = {
    operation.operation match {
      case CREATE => createHandler
      case DELETE => deleteHandler
    }
  }
}
