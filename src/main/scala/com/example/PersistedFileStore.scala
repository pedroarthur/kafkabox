package com.example

import java.nio.file._
import java.util.concurrent.{Executors, TimeUnit}
import javax.annotation.{PostConstruct, PreDestroy}
import javax.inject.Inject

import org.apache.kafka.clients.producer.RecordMetadata
import org.javaswift.joss.model.StoredObject
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.stereotype.Component

import scala.collection.JavaConversions._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.Try

@Configuration class FileWatcherConfiguration {
  @Value("${file.watcher.path:${user.home}}") val pathToWatch = System.getProperty("user.home")

  @Bean def directory: Path = Paths.get(pathToWatch)
}

@Configuration class NioFileEventSourceConfiguration {
  @Bean def watchService: WatchService =
    FileSystems.getDefault.newWatchService()

  @Bean def watchKey(directory: Path): WatchKey =
    directory.register(watchService,
      StandardWatchEventKinds.ENTRY_CREATE,
      StandardWatchEventKinds.ENTRY_DELETE)
}

/**
  * TODO: a wonderful description about this nice piece of work!
  */
trait FileEventSource {
  def poll(): List[(Path, WatchEvent[Path])]
  def reset(): Unit
}

@Component class NioFileEventSource @Inject()
  (watchService: WatchService, watchKey: WatchKey) extends FileEventSource {

  val dir = watchKey.watchable().asInstanceOf[Path]

  def poll(): List[(Path, WatchEvent[Path])] = Try {
    watchService.take().pollEvents().toList.map(_.asInstanceOf[WatchEvent[Path]]) map {
      w => (dir.resolve(w.context()), w)
    }
  } recover {
    case t: RuntimeException => List()
  } get

  override def reset(): Unit = watchKey.reset()
}

@Component class IgnoreCache {
  val logger = LoggerFactory.getLogger(classOf[IgnoreCache])

  val ignoreCache = collection.mutable.Set[String]()

  def ignoreOnce(path: String) = ignoreCache.add(path)
  def shallIgnore(path: String) = ignoreCache.remove(path)
}

@Component class FileWatchingLoop @Inject() (fileEventSource: FileEventSource,
  preStorage: PersistedFileStore, postStorage: StoredObjectVisitor, ignoreCache: IgnoreCache) extends Runnable {

  val logger = LoggerFactory.getLogger(classOf[FileWatchingLoop])
  val executor = Executors.newSingleThreadScheduledExecutor()

  implicit val ec: ExecutionContext =
    ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  val preStorageOperation = Map(
    StandardWatchEventKinds.OVERFLOW -> overflowOperation _,
    StandardWatchEventKinds.ENTRY_CREATE -> preStorage.create _,
    StandardWatchEventKinds.ENTRY_DELETE -> preStorage.delete _)

  val postStorageOperation = Map(
    StandardWatchEventKinds.ENTRY_CREATE -> postStorage.create _,
    StandardWatchEventKinds.ENTRY_DELETE -> postStorage.delete _)

  @PostConstruct def setUp(): Unit = executor.scheduleAtFixedRate(this, 0L, 1L, TimeUnit.SECONDS)
  @PreDestroy def tearDown(): Unit = executor.shutdown()

  override def run(): Unit = Try {
    for {
      (path, event) <- fileEventSource.poll()
        if !ignoreCache.shallIgnore(path.toAbsolutePath.toString)
    } yield for {
      obj <- preStorageOperation(event.kind()).apply(path)
      log <- postStorageOperation(event.kind()).apply(obj)
    } yield log
  } recover {
    case t: Throwable => logger.error(s"error", t)
  } map {
    _ => fileEventSource.reset()
  }

  def overflowOperation(path: Path): Future[StoredObject] =
    Future {
      throw new IllegalStateException("overflowed")
    }
}
