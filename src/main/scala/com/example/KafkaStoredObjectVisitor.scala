package com.example

import java.util.concurrent.Executors
import javax.inject.Inject

import org.apache.kafka.clients.producer.{Producer, ProducerRecord, RecordMetadata}
import org.javaswift.joss.model.StoredObject
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import spray.json._
import fommil.sjs.FamilyFormats._

case class StorageOperation(operation: String, groupId: String)

object StorageOperationProtocol {
  import DefaultJsonProtocol._

  implicit val format = jsonFormat2(StorageOperation)
}

object StorageOperations {
  val CREATE: String = "CREATE"
  val DELETE: String = "DELETE"

  def create(group: String): StorageOperation = StorageOperation(CREATE, group)
  def delete(group: String): StorageOperation = StorageOperation(DELETE, group)
}

trait StoredObjectVisitor {
  def create(obj: StoredObject): Future[RecordMetadata]
  def delete(obj: StoredObject): Future[RecordMetadata]
}

/**
  * TODO: a wonderful description about this nice piece of work!
  */
@Component class KafkaStoredObjectVisitor @Inject()(producer: Producer[String, String],
  groupId: ConsumerParameters, topicConfiguration: KafkaTopicConfiguration)
  extends StoredObjectVisitor {

  val logger = LoggerFactory.getLogger(classOf[KafkaStoredObjectVisitor])

  implicit val ec: ExecutionContext =
    ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  override def create(obj: StoredObject): Future[RecordMetadata] =
    execute(obj, StorageOperations.create)

  override def delete(obj: StoredObject): Future[RecordMetadata] =
    execute(obj, StorageOperations.delete)

  def execute(obj: StoredObject, operation: String => StorageOperation): Future[RecordMetadata] = {
    val storageOperation = operation(groupId.groupId)
    Future {
      producer.send(new ProducerRecord[String, String](
        topicConfiguration.topic, obj.getName, storageOperation.toJson.compactPrint)).get()
    } andThen {
      case Success(_) => logger.info(s"${obj.getName} $storageOperation written to topic")
      case Failure(t) => logger.error("while writing on topic", t)
    }
  }
}
