package com.example

import java.nio.file.Path
import java.util.concurrent.Executors
import javax.inject.Inject

import org.javaswift.joss.model.{Account, Container, StoredObject}
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait ObjectStore {
  def get(name: String): Future[StoredObject]
}

/**
  * TODO: a wonderful description about this nice piece of work!
  */
trait PersistedFileStore {
  def create(path: Path): Future[StoredObject]
  def delete(path: Path): Future[StoredObject]
}

/**
  * TODO: a wonderful description about this nice piece of work!
  */
@Component class SwiftPersistedFileStore @Inject() (swiftContainer: Container, directory: Path)
  extends PersistedFileStore with ObjectStore {


  implicit val ec: ExecutionContext =
    ExecutionContext.fromExecutor(Executors.newFixedThreadPool(2))

  val logger = LoggerFactory.getLogger(classOf[SwiftPersistedFileStore])

  override def delete(path: Path): Future[StoredObject] =
    Future {
      val obj = swiftContainer.getObject(path.getFileName.toString)

      if (obj.exists()) {
        obj.delete()
      } else {
        throw new NoSuchElementException(path.toString)
      }

      obj
    } andThen {
      case Success(s) => logger.info(s"deleted $s")
      case Failure(t) => logger.error(s"while deleting $path", t)
    }

  override def create(path: Path): Future[StoredObject] =
    Future {
      val obj = swiftContainer.getObject(directory.relativize(path).toString)

      if (obj.exists()) {
        throw new IllegalStateException("file exists in server")
      } else {
        obj.uploadObject(path.toAbsolutePath.toFile)
      }

      obj
    } andThen {
      case Success(s) => logger.info(s"created $s")
      case Failure(t) => logger.error(s"while creating $path", t)
    }

  override def get(name: String): Future[StoredObject] =
    Future {
      swiftContainer.getObject(name)
    }
}
