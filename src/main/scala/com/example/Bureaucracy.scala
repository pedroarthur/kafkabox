package com.example

import org.springframework.boot.{SpringApplication, CommandLineRunner}
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication class Bureaucracy extends CommandLineRunner {
  override def run(args: String*): Unit = synchronized { wait() }
}

object Bureaucracy {
  def main(args: Array[String]): Unit =
    SpringApplication run classOf[Bureaucracy]
}
