package com.example

import org.javaswift.joss.client.factory.{AccountConfig, AccountFactory}
import org.javaswift.joss.model.{Container, Account}
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.{Bean, Configuration}

@Configuration class OpenStackConfiguration {

  @Value("${openstack.user.name:demo}") val username = ""
  @Value("${openstack.user.password:102030}") val password = ""
  @Value("${openstack.auth.url:http://192.168.1.200:5000/v2.0/tokens}") val authUrl = ""
  @Value("${openstack.tenant.name:demo}") val tenantName = ""
  @Value("${openstack.container.name:files}") val swiftContainer: String = ""

  @Bean def accountConfig: AccountConfig =
    new AccountConfig() {
      setUsername(username)
      setPassword(password)
      setAuthUrl(authUrl)
      setTenantName(tenantName)
    }

  @Bean def account(config: AccountConfig): Account = new AccountFactory(config).createAccount()

  @Bean def container(account: Account) = account.getContainer("files")

}
