package com.example

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, Producer}
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.{Bean, Configuration}

/**
  * TODO: a wonderful description about this nice piece of work!
  */
@Configuration class KafkaProducerConfiguration {

  @Value("${kafka.bootstrap.servers:localhost:9092}") val bootstrapServers = ""
  @Value("${kafka.producer.request.require.ack:1}") val requireAcknowledges = ""
  @Value("${kafka.topic:files}") val topic: String = ""

  @Bean def producer: Producer[String, String] =
    new KafkaProducer[String, String](new Properties() {
        put("bootstrap.servers", bootstrapServers)
        put("key.serializer", classOf[StringSerializer].getName)
        put("value.serializer", classOf[StringSerializer].getName)
        put("request.require.acks", requireAcknowledges)
      })

  @Bean def topicConfiguration = KafkaTopicConfiguration(topic)
}

case class KafkaTopicConfiguration(topic: String)
